// const firstName = document.querySelector('#fname');
// const lastName = document.querySelector('#lname');
// const fullName = document.querySelector('#fullname');


// // firstName.addEventListener('keyup', (event)=> {
// // 	// console.log(event.target);
// // 	// console.log(event.target.value)
// // })

// // lastName.addEventListener('keyup', (event)=> {
// // 	// console.log(event.target);
// // 	// console.log(event.target.value)
// // })


// const update = (name, last) => {	
//   let name = firstName.value;
//   let last = lastName.value;
//   let result = name + ' ' + last;
//   console.log(result);
// }


// fullName.addEventListener('keyup', update);


const firstName = document.querySelector('#fname');
const lastName = document.querySelector('#lname');
const fullName = document.querySelector('#fullname');

const updateFullName = () => {	
  let name = firstName.value;
  let last = lastName.value;
  fullName.textContent = name + ' ' + last;
};

firstName.addEventListener('keyup', updateFullName);

lastName.addEventListener('keyup', updateFullName);
